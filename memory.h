/*
 * FILE: memory.h
 *
 *  Created on: Sep 25, 2015
 *      Author: Duc Cuong Dinh
 *      		Jie Zhange
 */

#ifndef MEMORY_H_
#define MEMORY_H_

void initial_memory(void);
void print_info_memory(void);
unsigned int * allocate(unsigned int size);
unsigned int deallocate(unsigned int * address);

#endif /* MEMORY_H_ */
